
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g
import subprocess
import os


def listener_for_tree(id, msg):
    if id == "list1" and len(msg) > 7:
        file = f"./res/LifeOfSaints/{msg}"
        if os.path.exists(file):
            text = open(file, mode='r', encoding='utf-8').read()
            g.mcxml_set_text("editor1", text)
            g.mcxml_set()
        else:
            print(f"'{file}' does not exist.")
    

def listener_drag(id, msg):
    if id == "bar":
        list = msg.split("|")
        g.mcxml_set_left(id, list[1])
        g.mcxml_set()


if __name__ == '__main__':
    p = subprocess.Popen("python ./res/init.py")
    r = p.wait()
    if r == 0:
        xml = open('app.xml', mode='r', encoding='utf-8').read()
        g.mcxml_listener_for_tree(listener_for_tree)
        g.mcxml_listener_drag(listener_drag)
        g.mcxml_loop(xml)
    else:
        print("'./res/init.py' does not exist.")
