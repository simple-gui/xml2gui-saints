import os

current_version = "0.1"
regenerate = False
version_file = "res/version.txt"
data_file = "res/data.zip"
output_folder = "res/"


def write(file, content):
    outstream = open(file, mode='w', newline='\n', encoding='utf-8')
    outstream.write(content)
    outstream.close()


if __name__ == '__main__':
    if not os.path.exists(version_file):
        regenerate = True
    else:
        text = open(version_file, mode='r', encoding='utf-8').read()
        if text != current_version:
            regenerate = True
        
    if regenerate:
        import zipfile
        with zipfile.ZipFile(data_file, 'r') as zip_ref:
            zip_ref.extractall(output_folder)
            
            
    write(version_file, current_version)
